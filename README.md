


Dependencies
------------

Make sure you add the following to /etc/apt/sources.list

https://launchpad.net/~georgekola/+archive/clang

* build-essential
* llvm
* clang
* libdispatch-dev
* libavahi-common-dev
* libgnutls-dev
* pkg-config
* libxslt1-dev
* libicu-dev
* libssl-dev
* libxml2-dev
* libjpeg-dev
* libtiff5-dev
* libcario-dev
* xorg-dev

Installation
------------
Simply run

	./setup

Examples
--------
helloworld -- Simple program that prints out 'Hello, world!'
